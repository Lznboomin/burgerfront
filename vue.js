var app = new Vue ({
    el: "#app",
    data: {
        allBurgers: [],
        burgerSchema: {
            name: null,
            price: null,
            rating: null,
            location: null
        },
        burger: {
            id: null
        }
    },
    mounted: function() {
        this.getAllBurgers()
    },
    methods: {
        getAllBurgers() {
            this.$http.get('http://localhost:3000/burgers').then(response => {
                this.allBurgers = response.body;
              }, response => {
                console.log("Mis korraldad poja");
              });
        },
        createBurger() {
            this.$http.post('http://localhost:3000/burgers', this.burgerSchema).then(response => {
                this.allBurgers.push(this.burgerSchema);
              }, response => {
                // error callback
              });
        },
        getburger() {
            this.$http.get('http://localhost:3000/burgers/:id').then(response => {
                // get body data
                this.allBurgers = response.body;
              }, response => {
                // error callback
              });
        },
        updateBurger() {
            this.$http.patch('http://localhost:3000/burgers/:id').then(response => {

            }, response => {

            });
        },
        deleteBurger(id) {
            console.log("tere");
            this.$http.delete('http://localhost:3000/burgers/' + id).then(response => {
                this.getAllBurgers = this.allBurgers.filter(function(burger){
                    console.log(burger._id != id)
                    return burger._id != id
                })
              }, response => {
                console.log("Seda burgerit ei eksisteeri");
              });
        }
    }
});